# Tracker - projet Simplon

Projet back-end réalisé par [Baptiste](https://gitlab.com/azdra), [Jean](https://gitlab.com/jean.mionnet.simplon), [Kévin](https://gitlab.com/kevin.wolff), [Lisa](https://gitlab.com/lmichallon).

## Installation avec Docker et docker-compose

### Pré-requis

Assurez-vous d'avoir Docker et docker-compose d'installé sur votre système.

Assuez-vous que le port xxxx, xxxx, sont disponibles sur votre système.

Selon le site de [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) assurez-vous d'avoir votre utilisateur actuel dans le groupe Docker.

```shell
sudo groupadd docker
sudo usermod -aG docker your-user
sudo newgroup docker
```

## Installation du projet

```shell
git clone https://gitlab.com/jean.mionnet.simplon/tracker
cp .env.dist .env
```

Lancer Docker.

```
docker-compose up
```

Accéder à l'application :

http://localhost:8000/welcome